/* config.h */ 

/* ports */
#define RIGHT_MOTOR NXT_PORT_A
#define LEFT_MOTOR NXT_PORT_B
#define LAMP NXT_PORT_C

#define LIGHT  NXT_PORT_S1
#define SONIC  NXT_PORT_S2
#define TOUCH  NXT_PORT_S3

/* states */
#define INIT 0
#define DARK 1
#define WHITE 2
#define COLLISION 3
#define BACK 4
