/* rover.c */ 
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"

/* own definitions */
#include "config.h"

/* OSEK declarations */
DeclareCounter(SysTimerCnt);
DeclareTask(LCDMonitor);

/* LEJOS OSEK hooks */
void ecrobot_device_initialize()
{
  ecrobot_set_light_sensor_active(LIGHT);
  ecrobot_init_sonar_sensor(SONIC);
}

void ecrobot_device_terminate()
{
  ecrobot_set_light_sensor_inactive(LIGHT);
  ecrobot_term_sonar_sensor(SONIC);
}

/* LEJOS OSEK hook to be invoked from an ISR in category 2 */
void user_1ms_isr_type2(void)
{
  StatusType ercd;

  ercd = SignalCounter(SysTimerCnt); /* Increment OSEK Alarm Counter */ 
  if(ercd != E_OK)
  {
    ShutdownOS(ercd);
  }
}

/*============================================================================
 * Task: LCDMonitor
 */
TASK(LCDMonitor)
{
  ecrobot_status_monitor("My Rover");

  TerminateTask();
}

/******************************** END OF FILE ********************************/

